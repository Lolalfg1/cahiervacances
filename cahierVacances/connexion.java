package cahiervacances;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LLAFAGE
 */
public class connexion extends javax.swing.JFrame {

    //initialisation des variables 
    int compteur = 3;
    String fichierUtiisateur = "C:\\Users\\LLAFAGE\\Documents\\NetBeansProjects\\cahierVacances\\src\\ressources\\utilisateurs.txt";

    public connexion() {
        initComponents();

        //initialisation du composant
        this.compteurTxt.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleFrame = new javax.swing.JLabel();
        userName = new javax.swing.JLabel();
        password = new javax.swing.JLabel();
        inputUserName = new javax.swing.JTextField();
        inputPassword = new javax.swing.JTextField();
        inscription = new javax.swing.JButton();
        connec = new javax.swing.JButton();
        compteurTxt = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        titleFrame.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleFrame.setText("Connexion");

        userName.setText("Nom d'utilisateur");

        password.setText("Mot de passe");

        inscription.setText("S'inscrire");
        inscription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inscriptionActionPerformed(evt);
            }
        });

        connec.setText("Se connecter");
        connec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connecActionPerformed(evt);
            }
        });

        compteurTxt.setText("jLabel1");

        jLabel1.setText("Exemple: dupont.jean");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titleFrame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(compteurTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(userName)
                        .addComponent(password))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(inscription)
                        .addGap(28, 28, 28)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(inputUserName)
                        .addComponent(inputPassword, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE))
                    .addComponent(connec))
                .addGap(24, 24, 24))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleFrame)
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userName)
                    .addComponent(inputUserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(password)
                    .addComponent(inputPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(compteurTxt)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inscription)
                    .addComponent(connec))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void inscriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inscriptionActionPerformed
        // TODO add your handling code here:
        new inscription().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_inscriptionActionPerformed

    private void connecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connecActionPerformed
        // verifie si le compteur n'est pas egale a 1
        if (this.compteur == 1) {
            //ferme la fenetre
            this.dispose();
        } else {

            //recuperation taille nom utilisateur
            String nomUtilisateur = this.inputUserName.getText();
            int recherchePoint = nomUtilisateur.indexOf(".");
            //decoupage du nom d'utilisateur
            String nom = nomUtilisateur.substring(0, recherchePoint);
            String prenom = nomUtilisateur.substring(recherchePoint + 1, nomUtilisateur.length());
            try {
                //verification du prenom et du nom dans le fihier text
                boolean reponse = RechercheUtilisateur(nom, prenom, this.inputPassword.getText());

                if (reponse == true) {
                    new gestionEleve().setVisible(true);
                    this.dispose();
                } else {
                    this.compteur--;
                    this.compteurTxt.setText("tentative restante: " + this.compteur);
                }
            } catch (IOException ex) {
                Logger.getLogger(connexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_connecActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(connexion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(connexion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(connexion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(connexion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new connexion().setVisible(true);
            }
        });
    }

    //recherche utilisateur dans le fichier text
    public boolean RechercheUtilisateur(String monNom, String monPrenom, String monMdp) throws FileNotFoundException, IOException {
        //ouvrir fichier
        File fichierUtilisateur = new File(this.fichierUtiisateur );
        FileReader lectureFichier = new FileReader(fichierUtilisateur);
        BufferedReader br = new BufferedReader(lectureFichier);
        String s;
        String[] mots = null;
        //Initialisation des mots a chercher 
        String inputNom = "nom:" + monNom + ";";
        String inputPrenom = "prenom:" + monPrenom + ";";
        String inputMdp = "mdp:" + monMdp + ";";

        int count = 0;
        //parcours du fichier
        while ((s = br.readLine()) != null) {
            mots = s.split(" ");
            //parcours la ligne
            for (String mot : mots) {
                //cherche les differentes string
                if (mot.equals(inputNom) || mot.equals(inputPrenom) || mot.equals(inputMdp)) {
                    count++;
                }
            }
        }
        //fermeture du fichier
        lectureFichier.close();

        if (count == 3) {
            return true;
        } else {
            return false;
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel compteurTxt;
    private javax.swing.JButton connec;
    private javax.swing.JTextField inputPassword;
    private javax.swing.JTextField inputUserName;
    private javax.swing.JButton inscription;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel password;
    private javax.swing.JLabel titleFrame;
    private javax.swing.JLabel userName;
    // End of variables declaration//GEN-END:variables
}
