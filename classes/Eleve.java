/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author LLAFAGE
 */
public class Eleve  extends Personne{
    private int id;
    private boolean sexe;

    public Eleve(int id, boolean sexe, String nom, String prenom) {
        super(nom, prenom);
        this.id = id;
        this.sexe = sexe;
    }

    public Eleve() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSexe() {
        return this.sexe;
    }

    public void setSexe(boolean sexe) {
        this.sexe = sexe;
    }
    
    
    
    
    
}
